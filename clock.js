// Canvas clock, Teppo Rekola 2019

function runClock() {
  clock(); //run at start once
  intervId = setInterval(clock, 100); //update 10Hz
}

function clock() {
  var now = new Date();
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');

  if (window.innerHeight < window.innerWidth ){
    canvas.height = window.innerHeight -20; //to fit without scrollbars
    canvas.width = canvas.height;
  }
  else {
    canvas.height = window.innerWidth -20; //to fit without scrollbars
    canvas.width = canvas.height;
  }

  var radius = canvas.height * 1.5;

  ctx.translate(canvas.width/2, canvas.height/2);   //start at center

  // static marks
  ctx.save();
  ctx.rotate(-Math.PI / 2);
  ctx.strokeStyle = 'yellow';
  ctx.lineWidth = canvas.height / 50;
  ctx.lineCap = 'round';
  for (var i = 0; i < 12; i++) {
    ctx.beginPath();
    ctx.rotate(Math.PI / 6);
    ctx.moveTo(radius/4 + ctx.lineWidth * 2, 0);
    ctx.lineTo(radius/4 + ctx.lineWidth * 2, 0);
    ctx.stroke();
  }
  ctx.restore();

  // TODO: data from websocket
  var sec = now.getSeconds();
  var min = now.getMinutes();
  var hr  = now.getHours();

  // format numbers for print
  if (sec < 10) {sec = "0" + sec};
  if (min < 10) {min = "0" + min};

  ctx.save();
  // TODO: font, color from data
  var fontSize = canvas.height / 6
  ctx.font = fontSize + "px sans-serif";
  //ctx.font = fontSize + "px Dotrice-Regular,sans-serif"; // why this wont work first frame after reload?
  ctx.textAlign = "center";
  ctx.fillStyle = "yellow";
  ctx.fillText(hr + ":" + min, 0, canvas.height / 10);
  ctx.fillText(sec, 0, canvas.height / 10 + fontSize);
  ctx.restore();

  // Write seconds
  ctx.save();
  ctx.rotate(-Math.PI / 2);

  if (sec < 60){
    for (var i = 0; i <= sec; i++) {
      ctx.strokeStyle = '#FF0000';
      ctx.lineWidth = canvas.height / 50;
      ctx.beginPath();
      ctx.moveTo(radius/4-ctx.lineWidth/8, 0);
      ctx.lineTo(radius/4+ctx.lineWidth/8, 0);
      ctx.lineCap = 'round';
      ctx.stroke();
      ctx.rotate( Math.PI / 30);
    }
  }
  ctx.restore();

}
