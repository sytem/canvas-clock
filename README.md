# canvas-clock

web clock / emulator for clock-8001: https://gitlab.com/Depili/clock-8001/

Teppo Rekola 2019

Canvas-code based on tutorial: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_animations
Dotrice font family by Paul Flo Williams ( https://www.1001fonts.com/dotrice-font.html ) is licensed under the SIL Open Font License (OFL)
